import json
import re
import contractions

import nltk.corpus
import pandas as pd

from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer


def read_events_txt(path: str, name: str):
    """
    Read data from .txt files.

    :param path: path to folder (without last slash). Eg: '/user/Downloads'
    :param name: filename to .txt file to read (without extension). Eg: 'train'
    :return: pandas df with data read

    Usage eg: read_events_txt(path='./data/Event_detection', name='train')
    """
    try:
        with open(f'{path}/{name}.txt', 'r') as file:
            lines = file.readlines()
            file.close()
    except OSError:
        file.close()
        print(f'Could not open/read file: {path}/{name}.txt')
        return pd.DataFrame()

    noticias = []
    eventos = []
    frases = []

    flag = lines[0].split('\t')[1].strip()
    noticia = 1
    frase = ''

    for line in lines:
        if line == '\n':
            frases.append(frase)
            eventos.append(flag)
            noticias.append(noticia)
            noticia = noticia + 1
            frase = ''
        else:
            palabra, evento = line.split('\t')
            if frase == '' and noticia > len(noticias):
                flag = evento.strip()
            if evento.strip() == flag:
                frase = ' '.join([frase, palabra])
            else:
                frases.append(frase)
                eventos.append(flag)
                noticias.append(noticia)
                frase = palabra
                flag = evento.strip()

    return pd.DataFrame({'noticia': noticias, 'frase': frases, 'evento': eventos})


def read_news_json(path: str, name: str):
    """
    Read data from .json files.

    :param path: path to folder (without last slash). Eg: '/user/Downloads'
    :param name: filename to .json file to read (without extension). Eg: 'train'
    :return: pandas df with data read

    Usage eg: read_news_json(path='./data/Trading_benchmark', name='evaluate_news')
    """
    try:
        with open(f'{path}/{name}.json', 'r') as file:
            data = json.loads(file.read())
            file.close()
    except OSError:
        print(f'Could not open/read file: {path}/{name}.json')
        file.close()
        return pd.DataFrame()

    # Flatten data
    return pd.io.json.json_normalize(data)


def normalize(string: str):
    """
    Converts string to lower case
    :param string: string to convert
    :return: lowercase string
    """
    return string.lower()


def unicode(string: str):
    """
    Eliminates punctuation, URL, and @ noise.
    :param string: string to fix
    :return: unicode string
    """
    return re.sub(r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)|^rt|http.+?", "", string)


def del_stopwords(txt: str):
    """
    Deletes stopwords from text parameter
    :param txt: string to clean
    :return: string without stopwords
    """
    nltk.download('stopwords')
    stop = stopwords.words('english')
    return " ".join([word for word in txt.split() if word not in stop])


def stemming(string: str):
    """
    Groups words by their root stem
    Eg: recognize that ‘jumping’ ‘jumps’ and ‘jumped’ are all rooted to the same verb (jump)
    :param string: string to group
    :return: root converted string
    """
    stemmer = PorterStemmer()
    return stemmer.stem(word=string)


def pos_tagger(tag: str):
    """
    Converts POS tag to wordnet tag
    :param tag: POS tag
    :return: wordnet tag
    """
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return None


def lemmatazing(txt: str):
    """
    Groups words based on root definition, and differentiates between present, past, and indefinite
    :param txt: text to lemmatize
    :return: root converted text
    """
    nltk.download('punkt')
    nltk.download('wordnet')
    nltk.download('averaged_perceptron_tagger')
    nltk_tagged = nltk.pos_tag(nltk.word_tokenize(txt))
    wordnet_tagged = map(lambda x: (x[0], pos_tagger(x[1])), nltk_tagged)
    lemmatized_sentence = []

    lemmatizer = WordNetLemmatizer()
    for word, tag in wordnet_tagged:
        if tag is None:
            lemmatized_sentence.append(word)
        else:
            lemmatized_sentence.append(lemmatizer.lemmatize(word, tag))
    return " ".join(lemmatized_sentence)


def rm_contraction(txt: str):
    """
    Converts english contractions into it full form
    Eg: I'd -> I would
    :param txt: text to be converted
    :return: text without contractions
    """
    return contractions.fix(txt)

